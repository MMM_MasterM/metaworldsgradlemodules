package metaworlds.miniature;

import metaworlds.api.SubWorld;
import metaworlds.api.WorldSuperClass;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.world.World;

public class BlockMiniaturizer extends Block {
	public BlockMiniaturizer()
    {
        super(Material.wood);
        this.setCreativeTab(CreativeTabs.tabBlock);
    }
	
	public void onBlockAdded(World par1World, int par2, int par3, int par4) 
	{
		if (!(par1World instanceof SubWorld))
			return;
		
		SubWorld subWorldPar = (SubWorld)par1World;
		double oldCenterX = subWorldPar.getCenterX();
		double oldCenterY = subWorldPar.getCenterY();
		double oldCenterZ = subWorldPar.getCenterZ();
		subWorldPar.setCenter((double)par2 + 0.5d, (double)par3 + 0.5d, (double)par4 + 0.5d);
		subWorldPar.setScaling(Math.max(0.01, subWorldPar.getScaling() * 0.5d));
		subWorldPar.setCenter(oldCenterX, oldCenterY, oldCenterZ);
	}
}
