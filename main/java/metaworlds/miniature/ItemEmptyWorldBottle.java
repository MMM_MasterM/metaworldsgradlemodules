package metaworlds.miniature;

import metaworlds.api.SubWorld;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class ItemEmptyWorldBottle extends Item {

	public ItemEmptyWorldBottle() {
		super();
		this.setMaxStackSize(64);
		this.setCreativeTab(CreativeTabs.tabTransport);
	}
	
	@Override
	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
		if (par2EntityPlayer.worldObj.isRemote)
			return true;
		
		if (par3World == null || !par3World.isSubWorld())
			return true;
		
		ItemStack newBottledWorld = new ItemStack(MetaworldsMiniatureMod.bottledWorldItem);
		
		newBottledWorld.setTagCompound(new NBTTagCompound());
		newBottledWorld.getTagCompound().setInteger("subWorldID", par3World.getSubWorldID());
		
		if (!par2EntityPlayer.inventory.addItemStackToInventory(newBottledWorld))
			return true;
		
		par2EntityPlayer.inventoryContainer.detectAndSendChanges();
		
		((SubWorld)par3World).removeSubWorld();
		
		par1ItemStack.stackSize--;
		if (par1ItemStack.stackSize <= 0)
			par2EntityPlayer.inventory.setInventorySlotContents(par2EntityPlayer.inventory.currentItem, null);
		
        return true;
    }
}
