package metaworlds.miniature;

import metaworlds.api.SubWorld;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class ItemBottledWorld extends Item 
{

	public ItemBottledWorld() {
		super();
		this.setMaxStackSize(1);
	}
	
	@Override
	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
		if (par2EntityPlayer.worldObj.isRemote)
			return true;
		
		if (!par1ItemStack.hasTagCompound())
			return true;
		
		if (par1ItemStack.getTagCompound().hasKey("subWorldID"))
		{
			int storedSubWorldID = par1ItemStack.getTagCompound().getInteger("subWorldID");
			
			World restoredWorld = par2EntityPlayer.worldObj.CreateSubWorld(storedSubWorldID);
			SubWorld restoredSubWorld = (SubWorld)restoredWorld;
			
			Vec3 destPos = par3World.transformToGlobal(par4 + par8, par5 + par9, par6 + par10);
			
			AxisAlignedBB newAABB = AxisAlignedBB.getAABBPool().getAABB(restoredSubWorld.getMinX(), restoredSubWorld.getMinY(), restoredSubWorld.getMinZ(), restoredSubWorld.getMaxX(), restoredSubWorld.getMaxY(), restoredSubWorld.getMaxZ());
			newAABB = newAABB.getTransformedToGlobalBoundingBox(restoredWorld);
			
			Vec3 posDiff = restoredWorld.getWorldVec3Pool().getVecFromPool(destPos.xCoord - (newAABB.maxX + newAABB.minX) * 0.5d, destPos.yCoord - newAABB.minY, destPos.zCoord - (newAABB.maxZ + newAABB.minZ) * 0.5d);
			
			restoredSubWorld.setTranslation(restoredSubWorld.getTranslationX() + posDiff.xCoord, restoredSubWorld.getTranslationY() + posDiff.yCoord, restoredSubWorld.getTranslationZ() + posDiff.zCoord);
			
			par1ItemStack.stackSize = 0;
			par2EntityPlayer.inventory.setInventorySlotContents(par2EntityPlayer.inventory.currentItem, null);
		}
		
        return true;
    }
}
