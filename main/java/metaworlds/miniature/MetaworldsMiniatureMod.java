package metaworlds.miniature;

import metaworlds.api.RecipeConfig;
import metaworlds.api.RecipeConfig.RecipePlaceHolderDef;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid=MetaworldsMiniatureMod.MODID, name="MetaworldsMiniatureMod", version=MetaworldsMiniatureMod.VERSION, dependencies="required-after:MetaworldsMod")
public class MetaworldsMiniatureMod {
	
	public static final String MODID = "metaworldsminiaturemod";
    public static final String VERSION = "0.995";
    
	public static Block miniaturizerBlock;
	public static RecipeConfig miniaturizerBlockConfig;
	public static Block scaleNormalizerBlock;
	public static RecipeConfig scaleNormalizerBlockConfig;
	public static Block supersizerBlock;
	public static RecipeConfig supersizerBlockConfig;
	public static Item emptyWorldBottleItem;
	public static RecipeConfig emptyWorldBottleItemConfig;
	public static Item bottledWorldItem;
	
	protected Configuration config;
	
    // The instance of your mod that Forge uses.
	//@Instance("MetaworldsMiniatureMod")
	//public static MetaworldsMiniatureMod instance;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	config = new Configuration(event.getSuggestedConfigurationFile());
    	
    	this.miniaturizerBlock = (new BlockMiniaturizer()).setHardness(0.5F).setStepSound(Block.soundTypeWood).setBlockName("subWorldMiniaturizer");
    	this.miniaturizerBlock.setBlockTextureName("metaworldsminiaturemod:" + this.miniaturizerBlock.getUnlocalizedName());
    	
    	this.scaleNormalizerBlock = (new BlockScaleNormalizer()).setHardness(0.5F).setStepSound(Block.soundTypeWood).setBlockName("subWorldScaleNormalizer");
    	this.scaleNormalizerBlock.setBlockTextureName("metaworldsminiaturemod:" + this.scaleNormalizerBlock.getUnlocalizedName());
    	
    	this.supersizerBlock = (new BlockSupersizer()).setHardness(0.5F).setStepSound(Block.soundTypeWood).setBlockName("subWorldSupersizer");
    	this.supersizerBlock.setBlockTextureName("metaworldsminiaturemod:" + this.supersizerBlock.getUnlocalizedName());
    	
    	this.emptyWorldBottleItem = new ItemEmptyWorldBottle().setUnlocalizedName("emptyWorldBottle");
    	this.emptyWorldBottleItem.setTextureName("potion_bottle_empty");
    	
    	this.bottledWorldItem = new ItemBottledWorld().setUnlocalizedName("bottledWorld");
    	this.bottledWorldItem.setTextureName("metaworldsminiaturemod:" + this.bottledWorldItem.getUnlocalizedName());
    	
    	config.load();
        
        this.miniaturizerBlockConfig = new RecipeConfig(config, "miniaturizerBlock", new ItemStack(this.miniaturizerBlock, 1, 0), false, new String[]{"G", "", ""}, new RecipePlaceHolderDef[]{new RecipePlaceHolderDef('G', Blocks.glass.getUnlocalizedName())});
        this.scaleNormalizerBlockConfig = new RecipeConfig(config, "scaleNormalizerBlock", new ItemStack(this.scaleNormalizerBlock, 1, 0), false, new String[]{"GGG", "GGG", "GGG"}, new RecipePlaceHolderDef[]{new RecipePlaceHolderDef('G', Blocks.gravel.getUnlocalizedName())});
        this.supersizerBlockConfig = new RecipeConfig(config, "supersizerBlock", new ItemStack(this.supersizerBlock, 1, 0), false, new String[]{"GGG", "GGG", "GGG"}, new RecipePlaceHolderDef[]{new RecipePlaceHolderDef('G', Blocks.glass.getUnlocalizedName())});
        this.emptyWorldBottleItemConfig = new RecipeConfig(config, "emptyWorldBottleItem", new ItemStack(this.emptyWorldBottleItem, 1, 0), true, new String[]{"GGG", "G G", "GGG"}, new RecipePlaceHolderDef[]{new RecipePlaceHolderDef('G', Blocks.glass_pane.getUnlocalizedName())});

        config.save();
    	
        GameRegistry.registerBlock(miniaturizerBlock, "subWorldMiniaturizer");
        GameRegistry.registerBlock(scaleNormalizerBlock, "subWorldScaleNormalizer");
        GameRegistry.registerBlock(supersizerBlock, "subWorldSupersizer");
        GameRegistry.registerItem(emptyWorldBottleItem, "emptyWorldBottle");
        GameRegistry.registerItem(bottledWorldItem, "bottledWorld");
        //LanguageRegistry.addName(miniaturizerBlock, "Miniaturization Block");
    	//LanguageRegistry.addName(scaleNormalizerBlock, "Scale Reset Block");
    	//LanguageRegistry.addName(supersizerBlock, "Supersizing Block");
    	//LanguageRegistry.addName(emptyWorldBottleItem, "Empty World Bottle");
    	//LanguageRegistry.addName(bottledWorldItem, "Bottled World");
    	miniaturizerBlockConfig.addRecipeToGameRegistry();
    	scaleNormalizerBlockConfig.addRecipeToGameRegistry();
    	supersizerBlockConfig.addRecipeToGameRegistry();
    	emptyWorldBottleItemConfig.addRecipeToGameRegistry();
    }
    
    @EventHandler
    public void load(FMLInitializationEvent event) {
    	
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
            // Stub Method
    }
}
