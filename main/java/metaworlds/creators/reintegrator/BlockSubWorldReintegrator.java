package metaworlds.creators.reintegrator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import metaworlds.api.SubWorld;
import metaworlds.api.WorldSuperClass;
import metaworlds.creators.contagious.BlockContagiousSubWorldCreator;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class BlockSubWorldReintegrator extends Block 
{
	public static Map<Integer, Boolean> blockVolatilityMap = new TreeMap<Integer, Boolean>();
	
	//public static boolean isBusy = false;//Prevent recursion

	public BlockSubWorldReintegrator() {
		super(Material.rock);
        this.setCreativeTab(CreativeTabs.tabBlock);
	}
	
	@Override
	public void onBlockAdded(World par1World, int par2, int par3, int par4) 
	{
		if (BlockContagiousSubWorldCreator.isBusy)
			return;
		
		BlockContagiousSubWorldCreator.isBusy = true;
		
		if (!par1World.isSubWorld())
			return;
		
		//Allign
		SubWorld subWorldPar = (SubWorld)par1World;
		double oldCenterX = subWorldPar.getCenterX();
		double oldCenterY = subWorldPar.getCenterY();
		double oldCenterZ = subWorldPar.getCenterZ();
		subWorldPar.setRotationYaw((double)Math.round(subWorldPar.getRotationYaw() / 90.0D) * 90.0D);
		subWorldPar.setRotationPitch(0.0D);
		subWorldPar.setRotationRoll(0.0D);
		subWorldPar.setTranslation(Math.round(subWorldPar.getTranslationX()), Math.round(subWorldPar.getTranslationY()), Math.round(subWorldPar.getTranslationZ()));
		subWorldPar.setScaling(1.0D);
		subWorldPar.setCenter(0.0D, 0.0D, 0.0D);
		
		subWorldPar.setMotion(0,  0,  0);
		
		subWorldPar.setRotationYawSpeed(0);
		subWorldPar.setRotationPitchSpeed(0);
		subWorldPar.setRotationRollSpeed(0);
		
		subWorldPar.setScaleChangeRate(0);
		
		byte facingDirection = (byte)(((Math.round(subWorldPar.getRotationYaw() / 90.0D) % 4) + 4) % 4);//positive remainder
		long translationX = Math.round(subWorldPar.getTranslationX());
		long translationY = Math.round(subWorldPar.getTranslationY());
		long translationZ = Math.round(subWorldPar.getTranslationZ());
		
		byte[] xzTransfMatrix = null;
		switch (facingDirection)
		{
			case 0:
				xzTransfMatrix = new byte[]{1, 0, 
											0, 1};
				break;
			case 1:
				xzTransfMatrix = new byte[]{0, 1, 
						-1, 0};
				translationZ -= 1;//offset because the block coordinates are not in the center of the blocks but in the corner!
				break;
			case 2:
				xzTransfMatrix = new byte[]{-1, 0, 
						0, -1};
				translationX -= 1;
				translationZ -= 1;
				break;
			case 3:
				xzTransfMatrix = new byte[]{0, -1, 
						1, 0};
				translationX -= 1;
				break;
		}
		
		
		
		List<BlockCoord3> blocksToTake = new ArrayList<BlockCoord3>();
		//Take in intelligent order so e.g. torches don't fall off the wall in the process
		List<BlockCoord3> blocksToTakeVolatile = new ArrayList<BlockCoord3>();
		
		HashSet<BlockCoord3> prevMargin = new HashSet<BlockCoord3>();
		HashSet<BlockCoord3> margin = new HashSet<BlockCoord3>();
		HashSet<BlockCoord3> newMargin = new HashSet<BlockCoord3>();
		
		blocksToTake.add(new BlockCoord3(par2, par3, par4));
		margin.add(new BlockCoord3(par2, par3, par4));
		
		boolean isValid = true;
		do
		{
			isValid = expandAtMargin(par1World, blocksToTake, blocksToTakeVolatile, prevMargin, margin, newMargin);
			if (!isValid)
				break;
			
			//Prepare for next loop
			HashSet<BlockCoord3> tmp = prevMargin;
			prevMargin = margin;
			margin = newMargin;
			newMargin = tmp;
			newMargin.clear();
		} while(margin.size() > 0);
		
		if (isValid)
		{
			World parentWorld = par1World.getParentWorld();
			
			//newSubWorld.setTranslation(par2, newSubWorld.getTranslationY(), par4);
			
			//1. Add non-volatile blocks
			for (BlockCoord3 curCoord : blocksToTake)
			{
				Block block = par1World.getBlock(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
				int blockMetadata = par1World.getBlockMetadata(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
				parentWorld.setBlock((int)(translationX + xzTransfMatrix[0] * curCoord.blockPosX + xzTransfMatrix[1] * curCoord.blockPosZ), (int)(translationY + curCoord.blockPosY), (int)(translationZ + xzTransfMatrix[2] * curCoord.blockPosX + xzTransfMatrix[3] * curCoord.blockPosZ), block, blockMetadata, 3);
			}
			
			//2. Add volatile blocks
			//3. Remove volatile blocks
			for (BlockCoord3 curCoord : blocksToTakeVolatile)
			{
				Block block = par1World.getBlock(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
				int blockMetadata = par1World.getBlockMetadata(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
				parentWorld.setBlock((int)(translationX + xzTransfMatrix[0] * curCoord.blockPosX + xzTransfMatrix[1] * curCoord.blockPosZ), (int)(translationY + curCoord.blockPosY), (int)(translationZ + xzTransfMatrix[2] * curCoord.blockPosX + xzTransfMatrix[3] * curCoord.blockPosZ), block, blockMetadata, 3);
			}
			
			for (BlockCoord3 curCoord : blocksToTakeVolatile)
			{
				par1World.setBlockToAir(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
			}
			
			//4. Remove non-volatile blocks
			for (BlockCoord3 curCoord : blocksToTake)
			{
				par1World.setBlockToAir(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
			}
		}
		//else if (par1World.isRemote)
		//{
		//	Minecraft.getMinecraft().thePlayer.sendChatToPlayer(ChatMessageComponent.createFromText("SubWorld touches bedrock!"));
		//}
		
		subWorldPar.setCenter(oldCenterX, oldCenterY, oldCenterZ);
		
		BlockContagiousSubWorldCreator.isBusy = false;
	}
	
	public boolean expandAtMargin(World par1World, List<BlockCoord3> blockList, List<BlockCoord3> volatileBlockList, HashSet<BlockCoord3> prevMarginList, HashSet<BlockCoord3> marginList, HashSet<BlockCoord3> newMarginList)
	{
		for (BlockCoord3 curCoord : marginList)
		{
			for (int direction = 0; direction < 6; ++direction)
			{
				BlockCoord3 newCoords = new BlockCoord3(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
				
				switch (direction / 2)
				{
					case 0:
						newCoords.blockPosY += (direction % 2) * 2 - 1;
						break;
					case 1:
						newCoords.blockPosX += (direction % 2) * 2 - 1;
						break;
					case 2:
						newCoords.blockPosZ += (direction % 2) * 2 - 1;
						break;
				}
				
				//TODO: connect diagonally
				/*
				 * On diagonal connections ignore if isBlockReplaceable
				 * or if block instanceof BlockFlower
				 * also don't connect diagonally FROM those blocks
				 */
				
				//Prevents loops and improves performance compared to old implementation
				if (prevMarginList.contains(newCoords) || marginList.contains(newCoords))
					continue;
				
				Block curBlock = par1World.getBlock(newCoords.blockPosX, newCoords.blockPosY, newCoords.blockPosZ); 
				if (curBlock.equals(Blocks.bedrock))
					return false;
				
				//Only take sources
				if (curBlock.equals(Blocks.air) || curBlock.equals(Blocks.flowing_water) || curBlock.equals(Blocks.flowing_lava))
					continue;
				
				//Only take tops of waterfalls
				if ((curBlock.equals(Blocks.water) || curBlock.equals(Blocks.lava)) && 
						par1World.getBlock(newCoords.blockPosX, newCoords.blockPosY + 1, newCoords.blockPosZ).equals(curBlock))
					continue;
				
				if (newMarginList.add(newCoords))
				{
				    int blockId = Block.getIdFromBlock(curBlock);
					Boolean isVolatile = blockVolatilityMap.get(blockId);
					
					if (isVolatile == null)
					{
						try
					    {
							if (curBlock.getClass().getMethod(BlockDummyReobfTracker.canBlockStayMethodName, World.class, int.class, int.class, int.class).getDeclaringClass().equals(Block.class) && 
									curBlock.getClass().getMethod(BlockDummyReobfTracker.onNeighborBlockChange, World.class, int.class, int.class, int.class, Block.class).getDeclaringClass().equals(Block.class))
							{
								isVolatile = false;
							}
							else
							{
								isVolatile = true;
							}
					    }
					    catch (SecurityException e)
					    {
					    }
					    catch (NoSuchMethodException e)
					    {
					    }
						
						blockVolatilityMap.put(blockId, isVolatile);
					}
					
					if (isVolatile)
						volatileBlockList.add(newCoords);
					else
						blockList.add(newCoords);
				}
			}
		}
		
		return true;
	}
	
	public class BlockCoord3
	{
		int blockPosX;
		int blockPosY;
		int blockPosZ;
		
		BlockCoord3(int x, int y, int z)
		{
			this.blockPosX = x;
			this.blockPosY = y;
			this.blockPosZ = z;
		}
		
		public boolean equals(Object par1Obj)
	    {
	        if (!(par1Obj instanceof BlockCoord3))
	        {
	            return false;
	        }
	        else
	        {
	        	BlockCoord3 targetCoord = (BlockCoord3)par1Obj;
	        	return targetCoord.blockPosX == this.blockPosX && targetCoord.blockPosY == this.blockPosY && targetCoord.blockPosZ == this.blockPosZ;
	        }
	    }

	    public int hashCode()
	    {
	        return this.blockPosY + (this.blockPosX + (this.blockPosZ << 12)) << 8;
	    }
	}
}
