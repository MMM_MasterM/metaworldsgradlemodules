package metaworlds.creators.reintegrator;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid=MetaworldsReintegratorMod.MODID, name="MetaworldsReintegratorMod", version=MetaworldsReintegratorMod.VERSION, dependencies="required-after:MetaworldsMod")
public class MetaworldsReintegratorMod {
	
	public static final String MODID = "metaworldsreintegratormod";
    public static final String VERSION = "0.995";
	
	public static Block subWorldReintegrator; 
	
	public static Block dummyBlock;

	// The instance of your mod that Forge uses.
	//@Instance("MetaworldsReintegratorMod")
	//public static MetaworldsReintegratorMod instance;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	this.subWorldReintegrator = (new BlockSubWorldReintegrator()).setHardness(3.0F).setResistance(15.0F).setStepSound(Block.soundTypeStone).setBlockName("subWorldReintegrator").setCreativeTab(CreativeTabs.tabBlock);
    	this.subWorldReintegrator.setBlockTextureName("planks_oak");
    	this.dummyBlock = new BlockDummyReobfTracker();
    	((BlockDummyReobfTracker)this.dummyBlock).initialize();
    	
    	GameRegistry.registerBlock(subWorldReintegrator, "subWorldReintegrator");
    }
}
