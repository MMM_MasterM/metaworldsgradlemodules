package metaworlds.creators.blank;

import metaworlds.api.RecipeConfig;
import metaworlds.api.RecipeConfig.RecipePlaceHolderDef;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid=MetaworldsBlankCreatorMod.MODID, name="MetaworldsBlankCreatorMod", version=MetaworldsBlankCreatorMod.VERSION, dependencies="required-after:MetaworldsMod")
public class MetaworldsBlankCreatorMod {
	
	public static final String MODID = "metaworldsblankcreatormod";
    public static final String VERSION = "0.995";
    
	//public static int blankSubWorldCreatorID;
	public static RecipeConfig blankSubWorldCreatorConfig;
	public static Block blankSubWorldCreator; 
	
	protected Configuration config;

    // The instance of your mod that Forge uses.
    //@Instance("MetaworldsBlankCreatorMod")
	//public static MetaworldsBlankCreatorMod instance;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	config = new Configuration(event.getSuggestedConfigurationFile());
    	
    	this.blankSubWorldCreator = (new BlockBlankSubWorldCreator()).setHardness(3.0F).setResistance(15.0F).setStepSound(Block.soundTypeStone).setBlockName("blankSubWorldCreator").setCreativeTab(CreativeTabs.tabBlock);
    	this.blankSubWorldCreator.setBlockTextureName("metaworldsblankcreatormod:" + this.blankSubWorldCreator.getUnlocalizedName());
    	
    	config.load();
        
        //this.blankSubWorldCreatorID = config.getBlock("blankSubWorldCreator", 3594).getInt();
        this.blankSubWorldCreatorConfig = new RecipeConfig(config, "blankSubWorldCreator", new ItemStack(this.blankSubWorldCreator, 1, 0), false, new String[]{"CCC", "CCC", "CCC"}, new RecipePlaceHolderDef[]{new RecipePlaceHolderDef('C', Blocks.cobblestone.getUnlocalizedName())});

        config.save();
    	
    	GameRegistry.registerBlock(blankSubWorldCreator, "blankSubWorldCreator");
    	//LanguageRegistry.addName(blankSubWorldCreator, "Blank SubWorld Creator");
    	blankSubWorldCreatorConfig.addRecipeToGameRegistry();
    }
    
    @EventHandler
    public void load(FMLInitializationEvent event) {
    	
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
            // Stub Method
    }
}
