package metaworlds.creators.contagious;

import metaworlds.api.RecipeConfig;
import metaworlds.api.RecipeConfig.RecipePlaceHolderDef;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid=MetaworldsContagiousCreatorMod.MODID, name="MetaworldsContagiousCreatorMod", version=MetaworldsContagiousCreatorMod.VERSION, dependencies="required-after:MetaworldsMod")
public class MetaworldsContagiousCreatorMod 
{
	public static final String MODID = "metaworldscontagiouscreatormod";
    public static final String VERSION = "0.995";
    
	//public static int contagiousSubWorldCreatorID;
	public static RecipeConfig contagiousSubWorldCreatorConfig;
	public static Block contagiousSubWorldCreator; 
	
	//public static int dummyBlockID;
	public static Block dummyBlock;
	
	protected Configuration config;

    // The instance of your mod that Forge uses.
	//@Instance("MetaworldsContagiousCreatorMod")
	//public static MetaworldsContagiousCreatorMod instance;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	config = new Configuration(event.getSuggestedConfigurationFile());
    	
    	this.contagiousSubWorldCreator = (new BlockContagiousSubWorldCreator()).setHardness(3.0F).setResistance(15.0F).setStepSound(Block.soundTypeStone).setBlockName("contagiousSubWorldCreator").setCreativeTab(CreativeTabs.tabBlock);
    	this.contagiousSubWorldCreator.setBlockTextureName("metaworldscontagiouscreatormod:" + this.contagiousSubWorldCreator.getUnlocalizedName());
    	this.dummyBlock = new BlockDummyReobfTracker();
    	((BlockDummyReobfTracker)this.dummyBlock).initialize();
    	
    	config.load();
        
        //this.contagiousSubWorldCreatorID = config.getBlock("contagiousSubWorldCreator", 3593).getInt();
        this.contagiousSubWorldCreatorConfig = new RecipeConfig(config, "contagiousSubWorldCreator", new ItemStack(this.contagiousSubWorldCreator, 1, 0), false, new String[]{"DDD", "DDD", "DDD"}, new RecipePlaceHolderDef[]{new RecipePlaceHolderDef('D', Blocks.dirt.getUnlocalizedName())});

        config.save();
    	
    	GameRegistry.registerBlock(contagiousSubWorldCreator, "contagiousSubWorldCreator");
    	//LanguageRegistry.addName(contagiousSubWorldCreator, "Contagious SubWorld Creator");
    	contagiousSubWorldCreatorConfig.addRecipeToGameRegistry();
    }
    
    @EventHandler
    public void load(FMLInitializationEvent event) {
    	
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
            // Stub Method
    }
}
