package metaworlds.creators.contagious;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import metaworlds.api.SubWorld;
import metaworlds.api.WorldSuperClass;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockContagiousSubWorldCreator extends Block 
{
	public static Map<Integer, Boolean> blockVolatilityMap = new TreeMap<Integer, Boolean>();
	
	public static boolean isBusy = false;//Prevent recursion
	
	public BlockContagiousSubWorldCreator()
    {
        super(Material.rock);
        this.setCreativeTab(CreativeTabs.tabBlock);
    }
	
	@Override
	public void onBlockAdded(World par1World, int par2, int par3, int par4) 
	{
		if (isBusy)
			return;
		
		isBusy = true;
		
		List<BlockCoord3> blocksToTake = new ArrayList<BlockCoord3>();
		//Take in intelligent order so e.g. torches don't fall off the wall in the process
		List<BlockCoord3> blocksToTakeVolatile = new ArrayList<BlockCoord3>();
		
		HashSet<BlockCoord3> prevMargin = new HashSet<BlockCoord3>();
		HashSet<BlockCoord3> margin = new HashSet<BlockCoord3>();
		HashSet<BlockCoord3> newMargin = new HashSet<BlockCoord3>();
		
		blocksToTake.add(new BlockCoord3(par2, par3, par4));
		margin.add(new BlockCoord3(par2, par3, par4));
		
		boolean isValid = true;
		do
		{
			isValid = expandAtMargin(par1World, blocksToTake, blocksToTakeVolatile, prevMargin, margin, newMargin);
			if (!isValid)
				break;
			
			//Prepare for next loop
			HashSet<BlockCoord3> tmp = prevMargin;
			prevMargin = margin;
			margin = newMargin;
			newMargin = tmp;
			newMargin.clear();
		} while(margin.size() > 0);
		
		if (isValid)
		{
			World newWorld = ((WorldSuperClass)par1World).CreateSubWorld();
			SubWorld newSubWorld = (SubWorld)newWorld;
			
			//newSubWorld.setTranslation(par2, newSubWorld.getTranslationY(), par4);
			
			//1. Add non-volatile blocks
			for (BlockCoord3 curCoord : blocksToTake)
			{
				Block block = par1World.getBlock(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
				int blockMetadata = par1World.getBlockMetadata(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
				newWorld.setBlock(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ, block, blockMetadata, 0);
				newWorld.setBlockMetadataWithNotify(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ, blockMetadata, 0);
				if (block.hasTileEntity(blockMetadata))
				{
					TileEntity origTE = par1World.getTileEntity(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
					NBTTagCompound nbttag = new NBTTagCompound();
					origTE.writeToNBT(nbttag);
					origTE.invalidate();
					TileEntity newTE = TileEntity.createAndLoadEntity(nbttag);
					newWorld.setTileEntity(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ, newTE);
				}
			}
			
			//2. Add volatile blocks
			//3. Remove volatile blocks
			for (BlockCoord3 curCoord : blocksToTakeVolatile)
			{
				Block block = par1World.getBlock(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
				int blockMetadata = par1World.getBlockMetadata(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
				newWorld.setBlock(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ, block, blockMetadata, 0);
				newWorld.setBlockMetadataWithNotify(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ, blockMetadata, 0);
				if (block.hasTileEntity(blockMetadata))
				{
					TileEntity origTE = par1World.getTileEntity(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
					NBTTagCompound nbttag = new NBTTagCompound();
					origTE.writeToNBT(nbttag);
					origTE.invalidate();
					TileEntity newTE = TileEntity.createAndLoadEntity(nbttag);
					newWorld.setTileEntity(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ, newTE);
				}
			}
			
			for (BlockCoord3 curCoord : blocksToTakeVolatile)
			{
				par1World.setBlockToAir(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
			}
			
			//4. Remove non-volatile blocks
			for (BlockCoord3 curCoord : blocksToTake)
			{
				par1World.setBlockToAir(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
			}
			
			newSubWorld.setCenter(par1World.getCenterX(), par1World.getCenterY(), par1World.getCenterZ());
			newSubWorld.setTranslation(par1World.getTranslationX(), par1World.getTranslationY(), par1World.getTranslationZ());
			newSubWorld.setRotationYaw(par1World.getRotationYaw());
			newSubWorld.setRotationPitch(par1World.getRotationPitch());
			newSubWorld.setRotationRoll(par1World.getRotationRoll());
			newSubWorld.setScaling(par1World.getScaling());
			
			newSubWorld.setCenter((double)par2 + 0.5d, (double)par3 + 0.5d, (double)par4 + 0.5d);
		}
		//else if (par1World.isRemote)
		//{
		//	Minecraft.getMinecraft().thePlayer.sendChatToPlayer(ChatMessageComponent.createFromText("SubWorld touches bedrock!"));
		//}
		
		isBusy = false;
	}
	
	public boolean expandAtMargin(World par1World, List<BlockCoord3> blockList, List<BlockCoord3> volatileBlockList, HashSet<BlockCoord3> prevMarginList, HashSet<BlockCoord3> marginList, HashSet<BlockCoord3> newMarginList)
	{
		for (BlockCoord3 curCoord : marginList)
		{
			for (int direction = 0; direction < 18; ++direction)
			{
				BlockCoord3 newCoords = new BlockCoord3(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
				
				//Plants or snow or other things that only sit as top layer on other blocks
				boolean includePlants = (direction == 1);
				boolean includeFromPlants = (direction == 0);
				
				if (direction < 6) //blocks sharing a side
				{
					switch (direction / 2)
					{
						case 0:
							newCoords.blockPosY += (direction % 2) * 2 - 1;
							break;
						case 1:
							newCoords.blockPosX += (direction % 2) * 2 - 1;
							break;
						case 2:
							newCoords.blockPosZ += (direction % 2) * 2 - 1;
							break;
					}
				}
				else //blocks sharing an edge
				{
					int dirXZ = direction - 6;
					
					if (dirXZ >= 4)//upper or lower four
					{
						dirXZ -= 4;
						
						if (dirXZ >= 4)
						{
							newCoords.blockPosY += 1; //upper four
							dirXZ -= 4;
						}
						else
							newCoords.blockPosY -= 1; //lower four
						
						switch (dirXZ / 2)
						{
							case 0:
								newCoords.blockPosX += (dirXZ % 2) * 2 - 1;
								break;
							case 1:
								newCoords.blockPosZ += (dirXZ % 2) * 2 - 1;
								break;
						}
					}
					else //middle four
					{
						newCoords.blockPosX += (dirXZ % 2) * 2 - 1;
						newCoords.blockPosZ += (dirXZ / 2) * 2 - 1;
					}
				}
				
				/*
				 * On diagonal connections ignore if isBlockReplaceable
				 * or if block instanceof BlockFlower
				 * also don't connect diagonally FROM those blocks
				 */
				
				//Prevents loops and improves performance compared to old implementation
				if (prevMarginList.contains(newCoords) || marginList.contains(newCoords))
					continue;
				
				//If the previous block was a plant only certain directions are allowed to connect to
				Block prevBlock = par1World.getBlock(curCoord.blockPosX, curCoord.blockPosY, curCoord.blockPosZ);
				if (!includeFromPlants && !prevBlock.getMaterial().isLiquid() && (prevBlock.getMaterial().isReplaceable() || prevBlock.getMaterial() == Material.plants))
					continue;
				
				Block curBlock = par1World.getBlock(newCoords.blockPosX, newCoords.blockPosY, newCoords.blockPosZ); 
				if (curBlock.equals(Blocks.bedrock))
					return false;
				
				//Only take sources
				if (curBlock.equals(Blocks.air) || curBlock.equals(Blocks.flowing_water) || curBlock.equals(Blocks.flowing_lava))
					continue;
				
				//Snow, plants and similar things are only allowed to be connected to from below
				if (!includePlants && !curBlock.getMaterial().isLiquid() && (curBlock.getMaterial().isReplaceable() || curBlock.getMaterial() == Material.plants))
					continue;
				
				//Only take tops of waterfalls
				if ((curBlock.equals(Blocks.water) || curBlock.equals(Blocks.lava)) && 
						par1World.getBlock(newCoords.blockPosX, newCoords.blockPosY + 1, newCoords.blockPosZ).equals(curBlock))
					continue;
				
				if (newMarginList.add(newCoords))
				{
				    int blockId = Block.getIdFromBlock(curBlock);
					Boolean isVolatile = blockVolatilityMap.get(blockId);
					
					if (isVolatile == null)
					{
						try
					    {
							if (curBlock.getClass().getMethod(BlockDummyReobfTracker.canBlockStayMethodName, World.class, int.class, int.class, int.class).getDeclaringClass().equals(Block.class) && 
									curBlock.getClass().getMethod(BlockDummyReobfTracker.onNeighborBlockChange, World.class, int.class, int.class, int.class, Block.class).getDeclaringClass().equals(Block.class))
							{
								isVolatile = false;
							}
							else
							{
								isVolatile = true;
							}
					    }
					    catch (SecurityException e)
					    {
					    }
					    catch (NoSuchMethodException e)
					    {
					    }
						
						blockVolatilityMap.put(blockId, isVolatile);
					}
					
					if (isVolatile)
						volatileBlockList.add(newCoords);
					else
						blockList.add(newCoords);
				}
			}
		}
		
		return true;
	}
	
	public class BlockCoord3
	{
		int blockPosX;
		int blockPosY;
		int blockPosZ;
		
		BlockCoord3(int x, int y, int z)
		{
			this.blockPosX = x;
			this.blockPosY = y;
			this.blockPosZ = z;
		}
		
		public boolean equals(Object par1Obj)
	    {
	        if (!(par1Obj instanceof BlockCoord3))
	        {
	            return false;
	        }
	        else
	        {
	        	BlockCoord3 targetCoord = (BlockCoord3)par1Obj;
	        	return targetCoord.blockPosX == this.blockPosX && targetCoord.blockPosY == this.blockPosY && targetCoord.blockPosZ == this.blockPosZ;
	        }
	    }

	    public int hashCode()
	    {
	        return this.blockPosY + (this.blockPosX + (this.blockPosZ << 12)) << 8;
	    }
	}
}
