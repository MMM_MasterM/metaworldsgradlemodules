package metaworlds.controls.captain;

import org.lwjgl.input.Keyboard;

import metaworlds.api.RecipeConfig;
import metaworlds.api.RecipeConfig.RecipePlaceHolderDef;
import metaworlds.core.GeneralPacketPipeline;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;

@Mod(modid=MetaworldsControlsCaptainMod.MODID, name="MetaworldsControlsCaptainMod", version=MetaworldsControlsCaptainMod.VERSION, dependencies="required-after:MetaworldsMod")
//@NetworkMod(clientSideRequired=true, serverSideRequired=true, channels={"mwcaptain"}, packetHandler = ControllerCaptainPacketHandler.class)
public class MetaworldsControlsCaptainMod {
	
	public static final String MODID = "metaworldscontrolscaptainmod";
    public static final String VERSION = "0.995";
    
	//public static int subWorldControllerID;
	public static RecipeConfig subWorldControllerConfig;
	public static Block subWorldController;
	Configuration config;
	
	public GeneralPacketPipeline networkHandler;
    
	public static final String CHANNEL = "mwcaptain";

    // The instance of your mod that Forge uses.
    @Instance("metaworldscontrolscaptainmod")
    public static MetaworldsControlsCaptainMod instance;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        
        config = new Configuration(event.getSuggestedConfigurationFile());
        
        this.subWorldController = (new BlockSubWorldController()).setHardness(0.5F).setStepSound(Block.soundTypeGravel).setBlockName("subWorldController");
    	this.subWorldController.setBlockTextureName("metaworldscontrolscaptainmod:" + this.subWorldController.getUnlocalizedName());
    	
    	config.load();

        this.subWorldControllerConfig = new RecipeConfig(config, "subWorldController", new ItemStack(this.subWorldController, 1, 0), false, new String[]{"PPP", "PPP", "PPP"}, new RecipePlaceHolderDef[]{new RecipePlaceHolderDef('P', Blocks.planks.getUnlocalizedName())});

        config.save();
        
        GameRegistry.registerBlock(subWorldController, "subWorldController");
    	//LanguageRegistry.addName(subWorldController, "SubWorld Controller");
    	subWorldControllerConfig.addRecipeToGameRegistry();
    	
    	EntityRegistry.registerModEntity(EntitySubWorldController.class, "EntitySubWorldController2", EntityRegistry.findGlobalUniqueEntityId(), this, 80, 3, true);
    }
    
    @EventHandler
    public void load(FMLInitializationEvent event) {
    	
        
        networkHandler = new GeneralPacketPipeline();
        networkHandler.initialize(CHANNEL);
        networkHandler.addDiscriminator(245, ControllerKeyUpdatePacket.class);
    	
    	if (event.getSide().isClient())
    	{
    	    FMLCommonHandler.instance().bus().register(new SubWorldControllerKeyHandler());
    	}
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
            // Stub Method
    }
}
