package metaworlds.controls.captain;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import metaworlds.core.MetaWorldsPacket;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetHandler;
import net.minecraft.network.NetHandlerPlayServer;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;

public class ControllerKeyUpdatePacket extends MetaWorldsPacket {
	private boolean ctrlDown;
	
	public ControllerKeyUpdatePacket()
	{ }
	
	public ControllerKeyUpdatePacket(boolean isCtrlDown)
	{
		this.ctrlDown = isCtrlDown;
	}
	
	@Override
    public void read(ChannelHandlerContext ctx, ByteBuf buf)
	{
	    this.ctrlDown = buf.readBoolean();
	}
	
	@Override
    public void write(ChannelHandlerContext ctx, ByteBuf buf)
	{
	    buf.writeBoolean(this.ctrlDown);
	}
	
	@Override
    public void execute(INetHandler netHandler, Side side, ChannelHandlerContext ctx)
    {
    	if (side.isClient())
    		return;
    	
    	EntityPlayer player = ((NetHandlerPlayServer)netHandler).playerEntity;
    	ControllerKeyServerStore keyStore = (ControllerKeyServerStore)player.getExtendedProperties("LCTRL");
    	if (keyStore == null)
    	{
    		keyStore = new ControllerKeyServerStore();
    		player.registerExtendedProperties("LCTRL", keyStore);
    	}
    	
    	keyStore.ctrlDown = this.ctrlDown;
    }
}
