package metaworlds.controls.captain;

import java.util.EnumSet;

import org.lwjgl.input.Keyboard;

import metaworlds.core.client.SubWorldClient;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.world.World;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.common.gameevent.InputEvent.KeyInputEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;

public class SubWorldControllerKeyHandler {
	
	public static boolean ctrl_down = false;
	
	protected KeyBinding lctrl_key;
	
	SubWorldControllerKeyHandler() 
	{
	    registerKeys();
	}
	
	public void registerKeys()
	{
	    //lctrl_key = new KeyBinding("LCTRL", Keyboard.KEY_LCONTROL, "Metaworlds");
        //ClientRegistry.registerKeyBinding(lctrl_key);
	}
	
	@SubscribeEvent
    public void onClientTick(TickEvent.ClientTickEvent event)
    {
        if (event.phase.equals(Phase.START))
        {
        	boolean ctrlKeyPressedNow = Minecraft.getMinecraft().gameSettings.keyBindSprint.getIsKeyPressed();
            //tickStart
            if (!ctrl_down && ctrlKeyPressedNow)
            {
                ctrl_down = true;
                MetaworldsControlsCaptainMod.instance.networkHandler.sendToServer(new ControllerKeyUpdatePacket(ctrl_down));
            }
            else if (ctrl_down && !ctrlKeyPressedNow)
            {
                ctrl_down = false;
                MetaworldsControlsCaptainMod.instance.networkHandler.sendToServer(new ControllerKeyUpdatePacket(ctrl_down));
            }
        }
    }
}
