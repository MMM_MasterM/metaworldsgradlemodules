package metaworlds.controls.captain;

import metaworlds.api.EntitySuperClass;
import metaworlds.api.WorldSuperClass;
import metaworlds.patcher.EntityPlayerProxy;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class BlockSubWorldController extends Block
{
    public BlockSubWorldController()
    {
        super(Material.ground);
        this.setCreativeTab(CreativeTabs.tabBlock);
    }
    
    @Override
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
    	if (par1World.isRemote)
    		return true;
    	
    	if (par5EntityPlayer.isRiding() && par5EntityPlayer.ridingEntity instanceof EntitySubWorldController)
    		return true;
    	
    	if (par5EntityPlayer instanceof EntityPlayerProxy)
    		par5EntityPlayer = ((EntityPlayerProxy)par5EntityPlayer).getRealPlayer();
    	
    	EntitySuperClass entityPlayerSuperClass = (EntitySuperClass)par5EntityPlayer;
    	
    	World entityParentWorld = par5EntityPlayer.worldObj;
    	
    	EntitySubWorldController controllerEntity = new EntitySubWorldController(entityParentWorld, par1World, par5EntityPlayer.posX, par5EntityPlayer.posY + 0.6d, par5EntityPlayer.posZ);
    	controllerEntity.setStartingYaw((float)par1World.getRotationYaw() + par5EntityPlayer.rotationYaw);
    	
    	
    	controllerEntity.setControlledWorld(par1World);
    	
    	controllerEntity.setWorldBelowFeet(par5EntityPlayer.getWorldBelowFeet());
    	
    	if (!entityParentWorld.isRemote)
        {
    		entityParentWorld.spawnEntityInWorld(controllerEntity);
        }
    	
    	controllerEntity.interactFirst(par5EntityPlayer);
    	
    	return true;
    }
}
