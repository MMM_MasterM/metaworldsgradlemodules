package metaworlds.controls.alignment;

import java.io.File;

import metaworlds.api.RecipeConfig;
import metaworlds.api.RecipeConfig.RecipePlaceHolderDef;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid=MetaworldsControlsAlignMod.MODID, name="MetaworldsControlsAlignMod", version=MetaworldsControlsAlignMod.VERSION, dependencies="required-after:MetaworldsMod")
public class MetaworldsControlsAlignMod {
	
	public static final String MODID = "metaworldscontrolsalignmod";
    public static final String VERSION = "0.995";
    
	public static RecipeConfig subWorldAlignerConfig;
	protected Configuration config;
	public static Block subWorldAligner; 

    // The instance of your mod that Forge uses.
    //@Instance("MetaworldsControlsAlignMod")
	//public static MetaworldsControlsAlignMod instance;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        config = new Configuration(event.getSuggestedConfigurationFile());
        
        this.subWorldAligner = (new BlockSubWorldAligner()).setHardness(0.5F).setStepSound(Block.soundTypeWood).setBlockName("subWorldAligner");
    	this.subWorldAligner.setBlockTextureName("metaworldscontrolsalignmod:" + this.subWorldAligner.getUnlocalizedName());

        config.load();
        
        this.subWorldAlignerConfig = new RecipeConfig(config, "subWorldAligner", new ItemStack(this.subWorldAligner, 1, 0), false, new String[]{"SSS", "SSS", "SSS"}, new RecipePlaceHolderDef[]{new RecipePlaceHolderDef('S', Blocks.sand.getUnlocalizedName())});

        config.save();
    	
        GameRegistry.registerBlock(subWorldAligner, "subWorldAligner");
    	//LanguageRegistry.addName(subWorldAligner, "SubWorld Aligner");
    	subWorldAlignerConfig.addRecipeToGameRegistry();
    }
    
    @EventHandler
    public void load(FMLInitializationEvent event) {
    	
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
            // Stub Method
    }
}
