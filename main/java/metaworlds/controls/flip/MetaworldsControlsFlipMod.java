package metaworlds.controls.flip;

import net.minecraft.block.Block;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid=MetaworldsControlsFlipMod.MODID, name="MetaworldsControlsFlipMod", version=MetaworldsControlsFlipMod.VERSION, dependencies="required-after:MetaworldsMod")
public class MetaworldsControlsFlipMod {
	
	public static final String MODID = "metaworldscontrolsflipmod";
    public static final String VERSION = "0.995";

    public static Block flipSubWorldBlock;
    
    //@Instance("MetaworldsControlsFlipMod")
    //public static MetaworldsControlsFlipMod instance;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	
    	this.flipSubWorldBlock = (new BlockFlipSubWorld()).setHardness(0.5F).setStepSound(Block.soundTypeStone).setBlockName("flipSubWorldBlock");
        //this.flipSubWorldBlock.setBlockTextureName("metaworldscontrolsalignmod:" + this.flipSubWorldBlock.getUnlocalizedName());
        this.flipSubWorldBlock.setBlockTextureName("planks_oak");
        
        GameRegistry.registerBlock(flipSubWorldBlock, "flipSubWorldBlock");
    }
    
    @EventHandler
    public void load(FMLInitializationEvent event)
    {
        
    }
}
