package metaworlds.controls.flip;

import metaworlds.api.SubWorld;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.world.World;

public class BlockFlipSubWorld extends Block {

    public BlockFlipSubWorld()
    {
        super(Material.rock);
        this.setCreativeTab(CreativeTabs.tabBlock);
    }

    @Override
    public void onBlockAdded(World par1World, int par2, int par3, int par4) 
    {
        if (!par1World.isSubWorld())
            return;
        
        SubWorld subWorldPar = (SubWorld)par1World;
        if (subWorldPar.getRotationPitch() == 0.0d)
            subWorldPar.setRotationPitch(180.0d);
        else
            subWorldPar.setRotationPitch(0.0d);
    }
}
